import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }  from '@angular/forms';

import { AppComponent } from './app.component';
import { RegistroComponent } from './registro/registro.component';

import { HttpClientModule } from '@angular/common/http';
import { BarraComponent } from './barra/barra.component';
import { RouterModule, Routes } from '@angular/router';
import { VerComponent } from './ver/ver.component';

const routes: Routes = [
  { path: 'registro', component: RegistroComponent ,},
  { path: 'ver', component: VerComponent ,},
];

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    BarraComponent,
    VerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
