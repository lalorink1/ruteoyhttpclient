import { Component, OnInit } from '@angular/core';
import {ConeccionService} from '../coneccion.service'
import {Persona} from './Persona'
@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit {

  Personas:Persona[];

  constructor(private con:ConeccionService) { }

  ngOnInit() {
   
    // this.con.SetConexion('/DatosUsuario',1,0);

    this.con.SetConexion("/DatosUsuario",1,0).subscribe(
      data=>{
       this.Personas=data
      },
      error=>{
        
      }
    );
  }


}
