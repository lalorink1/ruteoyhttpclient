import { Component, OnInit } from '@angular/core';
import {Persona} from '../ver/Persona';
import {ConeccionService} from '../coneccion.service'
import {NgForm} from '@angular/forms';
import {Modelos} from '../Modelos'
import {Router} from '@angular/router'
declare var $:any;

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {

  
  
  constructor(private con:ConeccionService,private route:Router) {
   }

  persona:Persona;

  ngOnInit() {
  }

  
  agregarPersona(persona:NgForm):void{   
    this.persona=persona.value; 
    
    this.con.SetConexion("/guardarusuario",2,this.persona).subscribe(
      response=>{
        persona.reset();
        $('.button').blur();
        this.route.navigate(['ver'])
      },
      error=>{
        console.log(<any>error)
      }
    );
  }



}
