import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Persona} from './ver//Persona'
import {  Observable } from 'rxjs';
// import {Modelos} from './Modelos'
import { Modelos} from './interface'

@Injectable({
  providedIn: 'root'
})
export class ConeccionService {
  
  private base="http://localhost:3333";
  private Url:string;
  private Index:number;
  private metodo:number;

  private Modelo:any;

  constructor(private httpclient:HttpClient,protected Model:Modelos) { }

 
  /**Selecciona tu conexion y peticion */
  public SetConexion(url:string,metodo:number,objeto:any){
    this.Url=this.base+url;
    return this.setMethodo(metodo,objeto);
  }

  private getDatos():Observable<any>{

    return  this.httpclient.get(this.Url)

  }

  private setMethodo(i:number,objeto:any){
    switch (i) {
      case 1:
        return  this.getDatos();
        break;
        case 2:
       return  this.addDatos(objeto);
        break;
    
      default:
        break;
    }
  }

  private addDatos(objeto:any):Observable<any>{

    return this.httpclient.post((this.Url), objeto,{
      headers:new HttpHeaders().set('Content-Type','application/json')
    });

    // return this.httpclient.post<Persona>((this.base+"/Agregar"),persona,{
    //   headers:new HttpHeaders().set('Content-Type','application/json')
    // });

  }
//  public getDatos(index:number){

//     this.modelo=this.Model.SetModelo(index);

//     this.httpclient.get(this.base+"/DatosUsuario").subscribe((resp:any[])=>{
//       this.modelo=resp;
//       console.log(this.modelo);
//     });
//     console.log(this.modelo);

//     return this.modelo;
//   }
setinica(){
  this.Url=this.base+"/DatosUsuario";

 // this.setModelo(this.persona);
 // console.log(this.Modelo);
 
 return this.getDatos();
}

// setModelo(nm:any){
//  this.Modelo=this.persona;
// }


}
