import {Persona} from './ver//Persona'
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class Modelos{

    modelo:any[]=[Persona]

    SetModelo(i:number):any[]{     
      return this.modelo[i]=[];
    } 

}